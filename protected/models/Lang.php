<?php

/**
 * This is the model class for table "lang".
 *
 * The followings are the available columns in table 'lang':
 * @property integer $id
 * @property string $name
 * @property string $group
 * @property string $label
 * @property string $hint
 * @property string $type
 */
class Lang extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Lang the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'lang';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, group, label, hint, type', 'required'),
			array('name, group, label', 'length', 'max'=>100),
			array('hint', 'length', 'max'=>200),
			array('type', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, group, label, hint, type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'group' => 'Group',
			'label' => 'Label',
			'hint' => 'Hint',
			'type' => 'Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('group',$this->group,true);
		$criteria->compare('label',$this->label,true);
		$criteria->compare('hint',$this->hint,true);
		$criteria->compare('type',$this->type,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getModelLang()
	{
		$model = array();
		$data = Lang::model()->findAll('1 ORDER BY `group`, label ASC');
		foreach ($data as $key => $value) {
			$modelDesc = array();
			foreach (Language::model()->getLanguage() as $k => $v) {
				$modelDesc[$v->code] = Lang::model()->getLangModel($value->name, $v->code);
				if ($modelDesc[$v->code]==null) {
					$modelDesc[$v->code] = new LangDescription;
				}
			}

			$model[] = array(
				'data'=>$value,
				'desc'=>$modelDesc,
			);
		}
		
		return $model;
	}

	public function getLang($lang)
	{
		$data = $this->findAll();
		$dataArray = array();
		foreach ($data as $key => $value) {
			$v = LangDescription::model()->getLangModel($value->name, $lang);
			$dataArray[$value->name]=$v->value;
		}
		
		return $dataArray;
	}

	public function getLangModel($lang_name, $language_code)
	{
		$lang_id = Lang::model()->find('name = :name',array(':name'=>$lang_name))->id;
		$language_id = Language::model()->find('code = :code',array(':code'=>$language_code))->id;
		$model = LangDescription::model()->find('lang_id = :lang_id AND language_id = :language_id', array(':lang_id'=>$lang_id, ':language_id'=>$language_id));
		return $model;
	}
}
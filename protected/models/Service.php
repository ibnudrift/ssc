<?php

/**
 * This is the model class for table "service".
 *
 * The followings are the available columns in table 'service':
 * @property integer $id
 * @property string $image
 * @property integer $active
 * @property string $date_input
 * @property string $date_update
 * @property string $insert_by
 * @property string $last_update_by
 */
class Service extends CActiveRecord
{
	public $title;
	public $content;
	public $category_name;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Service the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'service';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('active, category', 'required'),
			array('active', 'numerical', 'integerOnly'=>true),
			array('image, insert_by, last_update_by', 'length', 'max'=>255),

			array('image', 'file', 'types'=>'jpg, gif, png', 'allowEmpty'=>FALSE, 'on'=>'insert'),
			array('image', 'file', 'types'=>'jpg, gif, png', 'allowEmpty'=>TRUE, 'on'=>'update'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('title ,id, category_name, image, active, date_input, date_update, insert_by, last_update_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category' => 'Category',
			'category_name' => 'Category',
			'title' => 'Title',
			'image' => 'Image',
			'active' => 'Active',
			'date_input' => 'Date Input',
			'date_update' => 'Date Update',
			'insert_by' => 'Insert By',
			'last_update_by' => 'Last Update By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($language_id)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->select = "t.*, service_description.title, service_category_description.category as category_name";
		$criteria->join = "
		LEFT JOIN service_description ON service_description.service_id=t.id
		LEFT JOIN service_category ON service_category.id=t.category
		LEFT JOIN service_category_description ON service_category_description.service_category_id=service_category.id

		";
		$criteria->addCondition('service_description.language_id = :language_id');
		$criteria->params = array(':language_id'=>$language_id);

		$criteria->compare('id',$this->id);
		$criteria->compare('image',$this->image,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('date_input',$this->date_input,true);
		$criteria->compare('date_update',$this->date_update,true);
		$criteria->compare('insert_by',$this->insert_by,true);
		$criteria->compare('last_update_by',$this->last_update_by,true);

		$criteria->compare('title',$this->title, true);
		$criteria->compare('service_category_description.category',$this->category_name, true);

		$criteria->order = "date_update DESC";

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getData($id, $languageId=1)
	{
		$criteria=new CDbCriteria;

		$criteria->select = "t.*, service_description.title, service_description.content";
		$criteria->join = "LEFT JOIN service_description ON service_description.service_id=t.id";
		$criteria->addCondition('service_description.language_id = :language_id');
		$criteria->addCondition('t.id = :id');
		$criteria->params = array(
			':language_id'=>$languageId,
			':id'=>$id,
		);

		$model = $this->find($criteria);

		return $model;
	}

	public function getDataDesc($id, $languageId=1)
	{
		$criteria=new CDbCriteria;

		$criteria->addCondition('language_id = :language_id');
		$criteria->addCondition('service_id = :id');
		$criteria->params = array(
			':language_id'=>$languageId,
			':id'=>$id,
		);

		$model = ServiceDescription::model()->find($criteria);

		return $model;
	}

	public function getServiceAll($limit = false, $id = false, $languageId = 1)
	{
		$criteria=new CDbCriteria;

		$criteria->select = "t.*, service_description.title as title, service_description.content as content, service_category_description.category as category_name";
		$criteria->join = "
			LEFT JOIN service_description ON service_description.service_id=t.id
			LEFT JOIN service_category_description ON service_category_description.service_category_id=t.category
		";

		$params = array();
		if ($id !== false) {
			$criteria->addCondition('t.id != :id');
			$criteria->limit = $limit;
			$params[':id'] = $id;
		}

		$criteria->addCondition('service_description.language_id = :language_id');
		$criteria->addCondition('service_category_description.language_id = :language_id');
		$params[':language_id'] = $languageId;
		$criteria->order = "t.category asc, date_update DESC";
		$criteria->params = $params;

		if ($limit !== false) {
			$criteria->limit = $limit;
		}
		
		$model = Service::model()->findAll($criteria);

		return $model;
	}

	public function getCategory($languageId = 1)
	{
		$criteria=new CDbCriteria;

		$criteria->select = "t.*";
		$criteria->join = "RIGHT JOIN service_category ON service_category.id=t.service_category_id";
		$criteria->addCondition('t.language_id = :language_id');
		// $criteria->addCondition('t.id = :id');
		$criteria->params = array(
			':language_id'=>$languageId,
			// ':id'=>$id,
		);
		$criteria->order = "t.category asc";
		$model = ServiceCategoryDescription::Model()->findAll($criteria);

		return $model;
	}

	public function getMenuByCategory($languageId=1, $category, $limit = false)
	{
		$criteria=new CDbCriteria;

		$criteria->select = "t.*, service_description.title as title, service_description.content as content";
		$criteria->join = "LEFT JOIN service_description ON service_description.service_id=t.id";
		$criteria->addCondition('service_description.language_id = :language_id');
		
		if ($limit !== false) {
			$criteria->limit = $limit;
		}

		$criteria->addCondition('t.category = :category');
		$criteria->params = array(
			':language_id'=>$languageId,
			':category'=>$category,
		);
		$model = $this->findAll($criteria);

		$data = array();
		foreach ($model as $key => $value) {
			$data[] = array(
				'label'=>$value->title, 
				'url'=>array('/layanan/view', 'id'=>$value->id, 'url'=>Slug::create($value->title), 'lang'=>Yii::app()->language), 
				'active'=>($value->id == $_GET['id'])? true: false,
			);
		}
		// exit;
		return $data;
	}

}
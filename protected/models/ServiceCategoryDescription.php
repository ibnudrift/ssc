<?php

/**
 * This is the model class for table "service_category_description".
 *
 * The followings are the available columns in table 'service_category_description':
 * @property integer $id
 * @property integer $language_id
 * @property string $title
 * @property string $content
 */
class ServiceCategoryDescription extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ServiceCategoryDescription the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'service_category_description';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('category', 'required'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('category', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category' => 'Category',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('language_id',$this->language_id);
		$criteria->compare('category',$this->category,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getNameByCategoryID($categoryid)
	{
		$criteria=new CDbCriteria;

		$criteria->select = "t.*";
		// $criteria->join = "LEFT JOIN service_category ON service_category.id=t.service_category_id";
		$criteria->addCondition('t.id = :categoryid');
		// $criteria->addCondition('t.id = :id');
		$criteria->params = array(
			':categoryid'=>$categoryid,
			// ':id'=>$id,
		);
		$model = $this->find($criteria);

		return $model;
	}
}
<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ReservationForm extends CFormModel
{
	public $first_name;
	public $last_name;
	public $email;
	public $office_number;
	public $mobile_number;
	public $preferred_day;
	public $preferred_time;
	public $address_2;
	public $city;
	public $state;
	public $body;

	// public $verifyCode;
	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
			// name, email, subject and body are required
			array('first_name, last_name, email, office_number ,mobile_number, preferred_day, preferred_time, address_2, city, state', 'required'),
			// array('name, email, jeniskelamin, umur, telp, subject,  ask', 'required', 'on'=>'ask'),
			// email has to be a valid email address
			array('email', 'email'),
			array('first_name, last_name, email, office_number ,mobile_number, preferred_day, preferred_time, address_2, city, state, body', 'safe'),
			// verifyCode needs to be entered correctly
			// array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements()),
		);
	}

	/**
	 * Declares customized attribute labels.
	 * If not declared here, an attribute would have a label that is
	 * the same as its name with the first letter in upper case.
	 */
	public function attributeLabels()
	{
		return array(
			'verifyCode'=>Yii::t('main', 'Verification Code'),

			'first_name'=>Yii::t('main', 'First Name'),
			'last_name'=>Yii::t('main', 'Last Name'),
			'email'=>Yii::t('main', 'Email Address'),
			'office_number'=>Yii::t('main', 'Office Number'),
			'mobile_number'=>Yii::t('main', 'Mobile Number'),
			'preferred_day'=>Yii::t('main', 'Preferred Day'),
			'preferred_time'=>Yii::t('main', 'Preferred Time'),
			'address_2'=>Yii::t('main', 'Address'),
			'city'=>Yii::t('main', 'City'),
			'state'=>Yii::t('main', 'State'),
			'body'=>Yii::t('main', 'Message'),
		);
	}
}
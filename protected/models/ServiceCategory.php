<?php

/**
 * This is the model class for table "service_category".
 *
 * The followings are the available columns in table 'service_category':
 * @property integer $id
 * @property string $image
 * @property integer $active
 * @property string $date_input
 * @property string $date_update
 * @property string $insert_by
 * @property string $last_update_by
 */
class ServiceCategory extends CActiveRecord
{
	public $category;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ServiceCategory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'service_category';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(

			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('category ,id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'category' => 'Category',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search($language_id)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->select = "t.*, service_category_description.category";
		$criteria->join = "LEFT JOIN service_category_description ON service_category_description.service_category_id=t.id";
		$criteria->addCondition('service_category_description.language_id = :language_id');
		$criteria->params = array(':language_id'=>$language_id);

		$criteria->compare('id',$this->id);

		$criteria->compare('category',$this->category, true);

		$criteria->order = "category ASC";

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function getData($id, $languageId=1)
	{
		$criteria=new CDbCriteria;

		$criteria->select = "t.*, service_category_description.category";
		$criteria->join = "LEFT JOIN service_category_description ON service_category_description.service_category_id=t.id";
		$criteria->addCondition('service_category_description.language_id = :language_id');
		$criteria->addCondition('t.id = :id');
		$criteria->params = array(
			':language_id'=>$language_id,
			':id'=>$id,
		);

		$model = ServiceCategory::model()->find($criteria);

		return $model;
	}

	public function getDataDesc($id, $languageId=1)
	{
		$criteria=new CDbCriteria;

		$criteria->addCondition('language_id = :language_id');
		$criteria->addCondition('service_category_id = :id');
		$criteria->params = array(
			':language_id'=>$languageId,
			':id'=>$id,
		);

		$model = ServiceCategoryDescription::model()->find($criteria);

		return $model;
	}
	public function getAllData($languageId=1)
	{
		$criteria=new CDbCriteria;

		$criteria->select = "t.*, service_category_description.category";
		$criteria->join = "LEFT JOIN service_category_description ON service_category_description.service_category_id=t.id";
		$criteria->addCondition('service_category_description.language_id = :language_id');
		$criteria->params = array(
			':language_id'=>$languageId,
		);

		$model = ServiceCategory::model()->findAll($criteria);

		return $model;
	}
}
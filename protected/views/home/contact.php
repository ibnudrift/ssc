<div class="content-inside-about prelatif">
	<div class="clear h140"></div>
	<div class="prelatif container padding-left-30">
		<div class="left breadcumb"><a href="<?php echo CHtml::normalizeUrl(array('home/index')); ?>">Home</a> &gt; <b>Contact Us</b></div>
		<div class="clear height-10"></div>
		<div class="clear"></div>
	</div>
	<div class="lines-green"></div>
	<div class="prelatif container margin-left-30">
		<div class="clear height-25"></div>

		<div class="row">
			<div class="col-md-3">
				<!-- /. start left content -->
				<div class="left-content">
					<div class="inside">

						<div class="info-leftcontact w205 text-content">
								<span class="title text-gothic">Surabaya Spine Clinic</span>
								<div class="clear height-15"></div>
								<p class="text-gothic"><?php echo nl2br($this->setting['alamat']) ?></p>

								<div class="clear"></div>

								<dl class="dl-horizontal info-pg-contact">
								  <dt><i class="icon-phone-footer"></i></dt>
								  <dd><?php echo ($this->setting['phone']) ?></dd>
								  <div class="clear"></div>

								  <dt><a href="mailto:<?php echo ($this->setting['email']) ?>"><i class="icon-email-footer"></i></a></dt>
								  <dd><a href="mailto:<?php echo ($this->setting['email']) ?>"><?php echo ($this->setting['email']) ?></a></dd>
								  <div class="clear"></div>
								  
								  <dt><a href="<?php echo ($this->setting['facebook']) ?>"><i class="icon-facebook-footer"></a></i></dt>
								  <dd><a href="<?php echo ($this->setting['facebook']) ?>">spine.surabaya</a></dd>
								</dl>

								<div class="clear height-20"></div>
								<p>Click here to know how to <br> get to our <a href="<?php echo CHtml::normalizeUrl(array('home/location')); ?>"><b>location</b></a></p>					

							<div class="clear"></div>
						</div>

						<div class="clear"></div>
					</div>
					<div class="clear"></div>
				</div>
				<!-- /. End left content -->
			</div>
			<div class="col-md-9">
				<!-- /. start right content -->
				<div class="right-content">
					<div class="text-content inside contact-content">
						<h1 class="title-toppages"><font style="font-weight: normal;">Contact</font></h1>
						<div class="clear height-3"></div>
						<h1 class="title-toppages">Surabaya Spine Clinic</h1>
						<div class="clear height-25"></div>
						<p class="text-gothic"><?php echo ($this->setting['contact_content']) ?></p>

						<div class="contact-form">
							<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
							    'type'=>'horizontal',
								'enableAjaxValidation'=>false,
								'clientOptions'=>array(
									'validateOnSubmit'=>false,
								),
								'htmlOptions' => array(
									'enctype' => 'multipart/form-data',
								),
							)); ?>
								<div class="height-10"></div>
								<?php echo $form->errorSummary($model); ?>
								<?php if(Yii::app()->user->hasFlash('success')): ?>
								    <?php $this->widget('bootstrap.widgets.TbAlert', array(
								        'alerts'=>array('success'),
								    )); ?>
								<?php endif; ?>
								
								<!-- name -->
								<div class="form-group">
								    <label class="col-sm-4 control-label">Name</label>
								    <div class="col-sm-7 padding-0">
								      <?php echo $form->textField($model, 'name', array('class'=>'form-control')); ?>
								    </div>
								</div>
								<!-- email_address -->
								<div class="form-group">
								    <label class="col-sm-4 control-label">Email</label>
								    <div class="col-sm-7 padding-0">
								      <?php echo $form->textField($model, 'email', array('class'=>'form-control')); ?>
								    </div>
								</div>
								<!-- subject -->
								<div class="form-group">
								    <label class="col-sm-4 control-label">Subject</label>
								    <div class="col-sm-7 padding-0">
								    	<?php echo $form->textField($model, 'subject', array('class'=>'form-control')); ?>
								    </div>
								</div>
								<!-- body -->
								<div class="form-group">
								    <label class="col-sm-4 control-label">Message</label>
								    <div class="col-sm-7 padding-0">
								    	<?php echo $form->textArea($model, 'body', array('class'=>'form-control')); ?>
								    </div>
								</div>

								<div class="form-group">
								    <label class="col-sm-4 control-label">&nbsp;</label>
								    <div class="col-sm-7 padding-0">
								      <div class="g-recaptcha" data-sitekey="6Ld-huwUAAAAACuNrMXHmqzdv3RywvTB2YAuCE_l"></div>
								    </div>
								</div>
								<div class="form-group">
								    <label class="col-sm-4 control-label">&nbsp;</label>
								    <div class="col-sm-7 padding-0">
								      <?php $this->widget('bootstrap.widgets.TbButton', array(
											'buttonType'=>'submit',
											// 'type'=>'primary',
											'label'=>'',
										)); ?>
								    </div>
								</div>

								<?php
								/*
								<div class="form-group">
								    <label class="col-sm-4 control-label"></label>
								    <div class="col-sm-7 padding-0">
										<?php $this->widget('CCaptcha', array(
											'imageOptions'=>array(
												'style'=>'margin-bottom: 0px; margin-right: 10px;',
											),
										)); ?>
									</div>
								</div>
								<!-- body -->
								<div class="form-group">
								    <label class="col-sm-4 control-label">Security code</label>
								    <div class="col-sm-7">
								      	<div class="left w137">
								      		<?php echo $form->textField($model, 'verifyCode', array('class'=>'form-control w137')); ?>
								      	</div>
								      	<div class="right w147"><?php $this->widget('bootstrap.widgets.TbButton', array(
											'buttonType'=>'submit',
											// 'type'=>'primary',
											'label'=>'',
										)); ?></div>
								    </div>
								</div>
								*/ ?>

								<?php // echo $form->textFieldRow($model,'verifyCode',array('class'=>'span5')); ?>

							<?php $this->endWidget(); ?>

							<div class="clear"></div>
						</div>

						<div class="clear height-35"></div>
						<div class="clear"></div>
					</div>
					<div class="clear"></div>
				</div>
				<!-- /. End right content -->
			</div>
		</div>

		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div class="clear"></div>
		<div class="back-bottom-fcs-grey"></div>

		<script src='https://www.google.com/recaptcha/api.js'></script>
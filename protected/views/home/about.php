<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/id_ID/all.js#xfbml=1&appId=578821772130895";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- Place this tag after the last +1 button tag. -->
<script type="text/javascript">
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/platform.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>

<div class="content-inside-about prelatif">
	<div class="clear h140"></div>
	<div class="prelatif container padding-left-30">
		<div class="left breadcumb"><a href="<?php echo CHtml::normalizeUrl(array('home/index')); ?>">Home</a> &gt; <b>About</b></div>
		<div class="clear height-10"></div>
		<div class="clear"></div>
	</div>
	<div class="lines-green"></div>
	<div class="prelatif container margin-left-30">
		<div class="clear height-25"></div>

		<!-- /. start left content -->
		<div class="row">
			<div class="col-md-3">
				<div class="left-content">
					<div class="inside">
						<div class="menu-left-inscontent">
							<ul>
								<li><a href="#" id="menuscrl_m" data-id="1">Overview</a></li>
								<li><a href="#" id="menuscrl_m" data-id="2">Vision & Mission</a></li>
								<li><a href="#" id="menuscrl_m" data-id="3">Doctor’s Profile</a></li>
							</ul>
						</div>
						<div class="clear"></div>
					</div>
					<div class="clear"></div>
				</div>
				<!-- /. End left content -->
				
			</div>
			<div class="col-md-9">
				<!-- /. start right content -->
				<div class="right-content">
					<div class="text-content inside">
						<span class="page">About</span>
						<div class="clear height-3"></div>
						<h1 class="title-toppages">Surabaya Spine Clinic</h1>
						<div class="clear height-15"></div>
						<div class="back-blue-contentabout">
							<div class="row">
								<div class="col-md-5">
									<div class="left text">
										<div class="center w322 tengah">
											"Let’s achieve Quality Life through Healthier Spine”
											<br class="clear">
											<span class="surabaya">Surabaya Spine Clinic</span>
											<div class="clear"></div>
											<div class="bs-share">
												<div class="s-facebook">
													<div class="fb-like" data-href="http://surabayaspineclinic.com/" data-width="52" data-layout="box_count" data-action="like" data-show-faces="false" data-share="false"></div>
												</div>
												<div class="s-tweet">
													        <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://surabayaspineclinic.com/" data-via="your_screen_name" data-lang="en" data-related="anywhereTheJavascriptAPI" data-count="vertical">Tweet</a>
															<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
												</div>
												<div class="s-google">
													<!-- Place this tag where you want the +1 button to render. -->
													<div class="g-plusone" data-size="tall"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-7">
									<div class="c-img">
										<img class="img-responsive" src="<?php echo Yii::app()->baseUrl; ?>/asset/images/ill-about-grandhumanhappy.jpg" alt="">
									</div>
								</div>
							</div>
							
							
							<div class="back-shadow-bottom-backbluecontent"></div>
						</div>
						<div class="clear height-30"></div>

						<div class="lines-green"></div>
						<div class="clear height-25"></div>
						<div class="ct_mrk_1">
							<h3 class="title-sub">Overview Surabaya Spine Clinic</h3>
							<div class="clear height-15"></div>
							<?php echo ($this->setting['about_overview']) ?>
						<div class="clear"></div>
						</div>
						<div class="clear height-15"></div>
						<div class="lines-green"></div>
						<div class="clear height-25"></div>
						<div class="row ct_mrk_2">
							<div class="col-xs-6">
								<h3 class="title-sub">Vision</h3>
								<div class="clear height-15"></div>
								<p class="t-vision w350"><?php echo ($this->setting['about_visi']) ?></p>
							</div>
							<div class="col-xs-6">
								<h3 class="title-sub">Mission</h3>
								<div class="clear height-15"></div>
								<?php echo ($this->setting['about_misi']) ?>
							</div>
						</div>

						<div class="clear height-30"></div>
						<div class="lines-green"></div>
						<div class="clear height-25"></div>
						<div class="box-doctor-profile ct_mrk_3">
							<h3 class="title-sub">Doctor's Profile</h3>
							<div class="clear height-20"></div>
							<div class="left w238">
								<img src="<?php echo Yii::app()->baseUrl; ?>/images/static/<?php echo nl2br($this->setting['doctor_foto']) ?>" alt="">
								<div class="clear height-10"></div>
								<div class="nm-doctor padding-left-20"><?php echo ($this->setting['doctor_name']) ?></div>
								<div class="clear"></div>
							</div>
							<div class="left w596">
								<?php echo ($this->setting['doctor_description']) ?>
							</div>
							<div class="clear height-20"></div>
								<div class="padding-left-20">
		<?php echo ($this->setting['doctor_detail']) ?>
								</div>
							<div class="clear"></div>
						</div>

						<div class="clear height-35"></div>
						<div class="clear"></div>
					</div>
					<div class="clear"></div>
				</div>
				<!-- /. End right content -->
			</div>
		</div>
		
		

		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div class="clear"></div>
<div class="back-bottom-fcs-grey"></div>
<div class="content-inside-about prelatif">
	<div class="clear h140"></div>
	<div class="prelatif container padding-left-30">
		<div class="left breadcumb"><a href="<?php echo CHtml::normalizeUrl(array('home/index')); ?>">Home</a> &gt; <b>Our Location</b></div>
		<div class="clear height-10"></div>
		<div class="clear"></div>
	</div>
	<div class="lines-green"></div>
	<div class="prelatif container margin-left-30">
		<div class="clear height-25"></div>

		<!-- /. start left content -->
		<div class="left w257 left-content">
			<div class="inside">

				<div class="info-leftcontact w205 text-content">
						<span class="title text-gothic">Surabaya Spine Clinic</span>
						<div class="clear height-15"></div>
						<p class="text-gothic"><?php echo nl2br($this->setting['alamat']) ?></p>

						<div class="clear"></div>

						<dl class="dl-horizontal info-pg-contact">
						  <dt><i class="icon-phone-footer"></i></dt>
						  <dd><?php echo ($this->setting['phone']) ?></dd>

						  <dt><a href="mailto:<?php echo ($this->setting['email']) ?>"><i class="icon-email-footer"></i></a></dt>
						  <dd><a href="mailto:<?php echo ($this->setting['email']) ?>"><?php echo ($this->setting['email']) ?></a></dd>
						  
						  <dt><a href="<?php echo ($this->setting['facebook']) ?>"><i class="icon-facebook-footer"></a></i></dt>
						  <dd><a href="<?php echo ($this->setting['facebook']) ?>">spine.surabaya</a></dd>
						</dl>

					<div class="clear"></div>
				</div>

				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
		<!-- /. End left content -->
		
		<!-- /. start right content -->
		<div class="left w842 right-content">
			<div class="text-content inside contact-content">
				<h1 class="title-toppages"><font style="font-weight: normal;">The Location of</font></h1>
				<div class="clear height-3"></div>
				<h1 class="title-toppages">Surabaya Spine Clinic</h1>
				<div class="clear height-25"></div>

				<?php echo ($this->setting['location_content']) ?>

				<div class="clear height-15"></div>				
				<div class="il-imges"><img src="<?php echo Yii::app()->baseUrl; ?>/asset/images/shuttle.jpg" alt=""></div>

				<div class="clear height-25"></div>

				<div class="clear"></div>
				<div class="box-google-maps">
					<div class="ins">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d126646.65244482076!2d112.67654640895601!3d-7.274042623201104!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd7fbdba1c75cc1%3A0xc78ff722c99117c3!2sSurabaya+Spine+Clinic+%2F+Dokter+Nyeri+Saraf+Leher+Punggung+Tulang+Belakang+Syaraf+Kejepit!5e0!3m2!1sid!2s!4v1493348914849" width="819" height="399" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div>

				<div class="clear height-30"></div>
				<div class="height-3"></div>

				<p class="text-gothic margin-bottom-5"><b>Location of Surabaya Spine Clinic inside Siloam Hospital Surabaya</b></p>
				<div class="clear"></div>

				<div class="box-maps-2">
					<img src="<?php echo Yii::app()->baseUrl; ?>/asset/images/map-location.jpg" alt="">
				</div>

				<div class="clear height-45"></div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
		<!-- /. End right content -->

		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div class="clear"></div>
		<div class="back-bottom-fcs-grey"></div>
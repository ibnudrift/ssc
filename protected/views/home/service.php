<div class="content-inside-about prelatif">
	<div class="clear h140"></div>
	<div class="prelatif container padding-left-30">
		<div class="left breadcumb">Home &gt; <b>Services & Facilities</b></div>
		<div class="clear height-10"></div>
		<div class="clear"></div>
	</div>
	<div class="lines-green"></div>
	<div class="prelatif container margin-left-30">
		<div class="clear height-25"></div>

		<!-- /. start left content -->
		<div class="left w257 left-content">
			<div class="inside w232">
				
				<div class="t-nws-detail text-gothic"><b>Layanan Kesehatan</b></div>
				<div class="clear height-10"></div>
				<div class="menu-left-inscontent">
					<ul>
						<?php foreach ($model['layanan_kesehatan'] as $key => $value): ?>
								<li><a href="<?php echo CHtml::normalizeUrl(array('home/servicedetail', 'id'=> $key, 'category'=>'layanan_kesehatan') ); ?>"><?php echo ucwords($value['title']) ?></a></li>
						<?php endforeach ?>
					</ul>
				</div>
				<div class="clear height-20"></div>
				<div class="t-nws-detail text-gothic"><b>Layanan Pendukung</b></div>
				<div class="clear height-10"></div>
				<div class="menu-left-inscontent">
					<ul>
						<?php foreach ($model['layanan_pendukung'] as $key => $value): ?>
								<li><a href="<?php echo CHtml::normalizeUrl(array('home/servicedetail', 'id'=> $key, 'category'=>'layanan_pendukung') ); ?>"><?php echo ucwords($value['title']) ?></a></li>
						<?php endforeach ?>
					</ul>
				</div>
				<div class="clear"></div>

			</div>
			<div class="clear"></div>
		</div>
		<!-- /. End left content -->
		
		<!-- /. start right content -->
		<div class="left w842 right-content">
			<div class="text-content inside">
				<h1 class="title-toppages"><font style="font-weight: normal;">Services & Facilities from</font></h1>
				<div class="clear height-3"></div>
				<h1 class="title-toppages">Surabaya Spine Clinic</h1>
				<div class="clear height-20"></div>

				<div class="list-item-facilities-data">
					<div class="row">
							<?php foreach ($model as $key => $value): ?>
								<?php if ($key != 'layanan_kesehatan'): ?>
									<div class="clear"></div>
									<div class="name_kategori text-gothic"><?php echo strtoupper('LAYANAN PENDUKUNG').':'; ?></div>
									<div class="clear height-20"></div>
								<?php else: ?>
									<div class="clear"></div>
									<div class="name_kategori text-gothic"><?php echo strtoupper('LAYANAN KESEHATAN').':'; ?></div>
									<div class="clear height-20"></div>
								<?php endif; ?>
								<?php foreach ($value as $key => $vlu): ?>
									<div class="col-xs-4">
										<div class="item prelatif">
											<div class="pic"><img src="<?php echo Yii::app()->baseUrl; ?>/asset/images/<?php echo $vlu['img'] ?>" alt=""></div>
											<div class="clear height-20"></div>
											<div class="w220 tengah text-gothic h115">
												<div class="title"><?php echo ucwords( $vlu['title'] ); ?></div>
												<div class="clear height-15"></div>
												<div class="desc"><?php echo $vlu['desc'] ?>...</div>
											</div>
											<div class="bc-readmore-item text-gothic"><a href="<?php echo CHtml::normalizeUrl(array('home/Servicedetail')); ?>">read more&nbsp;&nbsp;&nbsp; <i class="icon-mr-bt-facilities-item"></i> </a></div>
											<div class="back-shadow-item-fclities"></div>
										</div>	
									</div>
								<?php endforeach ?>
							<?php endforeach ?>
					</div>
				</div>

				<div class="clear height-35"></div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
		<!-- /. End right content -->

		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div class="clear"></div>
		<div class="back-bottom-fcs-grey"></div>
<div class="content-inside-about prelatif">
	<div class="clear h140"></div>
	<div class="prelatif container padding-left-30">
		<div class="left breadcumb">Home &gt; <b>Services & Facilities</b></div>
		<div class="clear height-10"></div>
		<div class="clear"></div>
	</div>
	<div class="lines-green"></div>
	<div class="prelatif container margin-left-30">
		<div class="clear height-25"></div>

		<!-- /. start left content -->
		<div class="left w257 left-content">
			<div class="inside w232">

				<div class="t-nws-detail text-gothic"><b>Layanan Kesehatan</b></div>
				<div class="clear height-10"></div>
				<div class="menu-left-inscontent">
					<ul>
						<?php foreach ($model['layanan_kesehatan'] as $key => $value): ?>
								<li><a href="<?php echo CHtml::normalizeUrl(array('home/servicedetail', 'id'=> $key, 'category'=>'layanan_kesehatan') ); ?>"><?php echo ucwords($value['title']) ?></a></li>
						<?php endforeach ?>
					</ul>
				</div>
				<div class="clear height-20"></div>
				<div class="t-nws-detail text-gothic"><b>Layanan Pendukung</b></div>
				<div class="clear height-10"></div>
				<div class="menu-left-inscontent">
					<ul>
						<?php foreach ($model['layanan_pendukung'] as $key => $value): ?>
								<li><a href="<?php echo CHtml::normalizeUrl(array('home/servicedetail', 'id'=> $key, 'category'=>'layanan_pendukung') ); ?>"><?php echo ucwords($value['title']) ?></a></li>
						<?php endforeach ?>
					</ul>
				</div>
				<div class="clear"></div>

			</div>
			<div class="clear"></div>
		</div>
		<!-- /. End left content -->
		
		<!-- /. start right content -->
		<div class="left w842 right-content">
			<div class="text-content inside det-articles">
				<h1 class="title-toppages"><font style="font-weight: normal;"><?php echo ucwords($data['title']); ?></font></h1>
				<div class="clear height-20"></div>
				<div class="mg-ct-left"><img src="<?php echo Yii::app()->baseUrl; ?>/asset/images/<?php echo $data['img'] ?>" alt=""></div>
				<?php echo $data['desc'] ?>

				<div class="clear height-30"></div>
				<div class="t-other-arc text-gothic">Read other articles & publications:</div>
				<div class="clear height-10"></div>
				<div class="lines-green"></div>
				<div class="clear height-15"></div>

				<div class="list-item-facilities-data">
					<div class="row">
							<?php foreach ($model['layanan_pendukung'] as $key => $value): ?>
								<div class="col-xs-4">
									<div class="item prelatif">
										<div class="pic"><img src="<?php echo Yii::app()->baseUrl; ?>/asset/images/<?php echo $value['img'] ?>" alt=""></div>
										<div class="clear height-20"></div>
										<div class="w220 tengah text-gothic h115">
											<div class="title"><?php echo strtoupper( $value['title'] ); ?></div>
											<div class="clear height-15"></div>
											<div class="desc"><?php echo $value['desc'] ?></div>
										</div>
										<div class="bc-readmore-item text-gothic"><a href="#">read more&nbsp;&nbsp;&nbsp; <i class="icon-mr-bt-facilities-item"></i> </a></div>
										<div class="back-shadow-item-fclities"></div>
									</div>	
							</div>
							<?php endforeach ?>
					</div>
				</div>

				<div class="clear height-35"></div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
		<!-- /. End right content -->

		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div class="clear"></div>
		<div class="back-bottom-fcs-grey"></div>
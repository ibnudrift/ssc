<?php 
$detect = new MobileDetect;
?>

<!-- jquery camera -->
<link rel='stylesheet' id='camera-css'  href='<?php echo Yii::app()->baseUrl; ?>/asset/js/camera/css/camera.css' type='text/css' media='all'> 
<script type='text/javascript' src='<?php echo Yii::app()->baseUrl; ?>/asset/js/camera/scripts/jquery.mobile.customized.min.js'></script>
<script type='text/javascript' src='<?php echo Yii::app()->baseUrl; ?>/asset/js/camera/scripts/jquery.easing.1.3.js'></script> 
<script type='text/javascript' src='<?php echo Yii::app()->baseUrl; ?>/asset/js/camera/scripts/camera.min.js'></script>
<script type='text/javascript'>
    jQuery(function($){
    	var n_width = $(window).width();

    	if (n_width < 980){

    		var nmob = $('.camera_wrap .camera_src div').attr('data-sn_mobile');
    		$('.camera_wrap .camera_src div').attr('data-src', nmob);

    		$('#camera_wrap_1').camera({
	            height: '100%',
	            fx        : 'simpleFade',
	            loader: 'bar',
	            pagination: false,
	            thumbnails: false,
	            hover: false,
	            opacityOnGrid: false,
	            mobileAutoAdvance: false,
	            playPause: false,
	            alignment: 'topCenter',
	        });

    	}else{
	        $('#camera_wrap_1').camera({
	            height: 'auto',
	            fx        : 'simpleFade',
	            loader: 'bar',
	            pagination: false,
	            thumbnails: false,
	            hover: false,
	            opacityOnGrid: false,
	            mobileAutoAdvance: false,
	            playPause: false,
	            alignment: 'topCenter',
	        });

    	}
    });
</script>

		<div class="fluid_container">
			<div class="camera_wrap camera_azure_skin" id="camera_wrap_1">
				<?php
				$slide = Slide::model()->findAll();
				?>
				<?php foreach ($slide as $key => $value): ?>
				<?php if ( $detect->isMobile() ): ?>
					<div data-src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(750,839, '/images/slide/'.$value->image2 , array('method' => 'adaptiveResize', 'quality' => '90')) ?>">
				</div>
				<?php else: ?>
					<div data-src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1919,1010, '/images/slide/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>">
				</div>
				<?php endif ?>
				<?php endforeach ?>
			</div>
		</div>
		<div class="clear"></div>
		
		<div class="content-home prelatif">
		
		<!-- /. text big in fcs	 -->
		<div class="prelatif container">
			<div class="abs-text-big-infcs">
				<div class="text ">
					<?php echo ($this->setting['home_title']) ?>
					<div class="py-2"></div>
					<a href="<?php echo CHtml::normalizeUrl(array('layanan/index', 'lang'=>Yii::app()->language)); ?>" class="btn btn-customs-green">SEE OUR SERVICES</a>
				</div>
			</div>
		</div>
		<!-- /. End text big in fcs	 -->
		<div class="nbxs_bottom_fcs_wa">
			<div class="prelatife container">
				<div class="in_texts text-center py-3">
					<div class="visible-xs">
						<img src="<?php echo $this->asset_url.'logo_wa_white.png' ?>" alt="" class="img-responsive d-inline align-middle mr-2">
						<p class="m-0 d-inline align-middle"><a href="https://wa.me/6282233227788">WHATSAPP</a></p>
					</div>
					<div class="hidden-xs">
						<img src="<?php echo $this->asset_url.'logo_wa_white.png' ?>" alt="" class="img-responsive d-inline align-middle mr-3"><p class="m-0 d-inline align-middle"><strong>WHATSAPP</strong> 0822 3322 7788 <a href="https://wa.me/6282233227788">(Click to Chat)</a></p>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- end fcs -->
	
	<div class="clear"></div>
	<?php if (Yii::app()->user->hasFlash('msg')): ?>
		<script type="text/javascript">
			$(document).ready(function() {
				alert('<?php echo Yii::app()->user->getFlash('msg') ?>');
			})
		</script>
	<?php endif ?>

	<section class="outer-home-block-1">
		<div class="prelatife container">
			<div class="inner_side content-text cst_text text-center color_white">
				<h5>Welcome to Surabaya Spine Clinic</h5>
				<p>Surabaya Spine Clinic services scope include spinal examinations, treatments and surgery for all problems related to the spine. With experience and various reputations that have been achieved, Surabaya Spine Clinic is led by Dr. Eko Agus Subagio as a spine specialist doctor who is very experienced in his field. You will be rest assured as you are in the right hands. Please consult to us via Whatsaap by clicking on our Whatsaap number.</p>
				<div class="clear clearfix"></div>
			</div>
		</div>
	</section>

	<section class="block-home-2 lists_outer_feature_spine">
		<div class="prelatife container py-4">
			<div class="insides py-5">

				<div class="py-3"></div>
				<div class="tops_title">
					<h2>Surabaya Spine Clinic Services For Spinal Problems</h2>
					<div class="clear"></div>
					<div class="py-1"></div>
					<div class="lines-blue"></div>
					<div class="py-4"></div>
				</div>
				<?php
					$layanan_home = Service::model()->getServiceAll(false, false, $this->languageID);

				?>
				<div class="lists_feature_data">
					<div class="row default">
						<?php foreach ($layanan_home as $key => $value): ?>
							<?php if (intval($value->category) == 1): ?>
							<div class="col-lg-3 col-md-3 col-xs-12" data-cat="<?php echo $value->category ?>">
								<div class="items_boxed">
									<div class="picture">
										<a href="<?php echo CHtml::normalizeUrl(array('/layanan/view', 'id'=>$value->id, 'url'=>Slug::create($value->title), 'lang'=>Yii::app()->language)); ?>">
											<img class="img-responsive" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(641,434, '/images/service/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $value->title ?>">
										</a>
									</div>
									<div class="infos">
										<h4><?php echo ucwords( $value->title ); ?></h4>
										<div class="py-1"></div>
										<a href="<?php echo CHtml::normalizeUrl(array('/layanan/view', 'id'=>$value->id, 'url'=>Slug::create($value->title), 'lang'=>Yii::app()->language)); ?>" class="btn btn-link btns_nmore">Learn More</a>
									</div>
								</div>
							</div>
							<?php endif ?>
						<?php endforeach ?>
					</div>
				</div>
				<div class="clear clearfix"></div>
			</div>
		</div>
	</section>

	<div class="blocks-outer-newshome">
		<div class="py-4"></div>
		<div class="prelatife container text-content news_home">
			<h1 class="title-toppages">Articles &amp; Publication</h1>
			<div class="clear height-10"></div>
			<div class="lines-greys"></div>
			<div class="clear height-30"></div>
			<?php
			$category = Category::model()->findAll();
			?>
			<!-- starts data home news -->
			<?php foreach ($category as $k => $v): ?>
			<?php
			$_GET['category'] = $v->id;
			?>
			<?php $data = Artikel::model()->getAllData(4, false, $this->languageID); ?>
			<?php if (count($data) > 0): ?>
				
			<div class="outers-list-datahome">
				<div class="row">
					<div class="col-xs-9">
						<div class="titles-category"><?php echo $v->name ?></div>	
					</div>
					<div class="col-xs-3">
						<a href="<?php echo CHtml::normalizeUrl(array('/artikel/index', 'category'=>$v->id)); ?>" class="btn btn-default see_other">SEE IN <?php echo $v->name ?></a>
					</div>
				</div> <div class="clear height-20"></div>
				<div class="list-item-facilities-data news_homedata">
					<div class="row">
							<?php foreach ($data as $key => $value): ?>
								<div class="col-md-3 col-xs-12">
									<div class="paddings">
										<div class="item prelatif">
											<div class="pic">
												<a href="<?php echo CHtml::normalizeUrl(array('/artikel/detail', 'id'=>$value->id, 'url'=>Slug::create($value->title))); ?>">
													<img class="img-responsive" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(641,434, '/images/artikel/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $value->title ?>">
												</a>
											</div>
											<div class="infos py-3">
												<a href="<?php echo CHtml::normalizeUrl(array('/artikel/detail', 'id'=>$value->id, 'url'=>Slug::create($value->title))); ?>"><h4><?php echo ucwords( $value->title ); ?></h4></a>
												<div class="py-1"></div>
												<a href="<?php echo CHtml::normalizeUrl(array('/artikel/detail', 'id'=>$value->id, 'url'=>Slug::create($value->title))); ?>" class="btn btn-link btns_nmore">Learn More</a>
											</div>
											<?php 
											/*<div class="clear height-20"></div>
											<div class="w220 tengah text-gothic h115">
												<div class="title"><?php echo ucwords( $value->title ); ?></div> <div class="clear"></div>
												<div class="desc d-none">
												<p>
													<?php echo substr(strip_tags($value->content), 0, 45).'...'; ?>
												</p>
											</div>
											</div>
											<div class="bc-readmore-item text-gothic"><a href="<?php echo CHtml::normalizeUrl(array('/artikel/detail', 'id'=>$value->id, 'url'=>Slug::create($value->title))); ?>">read more&nbsp;&nbsp;&nbsp; <i class="icon-mr-bt-facilities-item"></i> </a></div>
											<div class="back-shadow-item-fclities"></div>
											*/ ?>
										</div>	
									</div>
								</div>
							<?php endforeach; ?>
					</div>
				</div>

				<?php if ((count($category) - 1) != $k ): ?>
					<div class="clear height-0"></div>
					<div data-count="<?php echo (count($data) - 1) ?>" data-key="<?php echo $k ?>" class="lines-greys"></div>
					<div class="clear height-30"></div>
					<div class="clear"></div>
				<?php else: ?>
					<div class="py-2"></div>
				<?php endif ?>
			</div>
			<?php endif ?>
			<?php endforeach ?>
			<?php
			$_GET['category'] = -1;
			?>

			<?php /*$data = Artikel::model()->getAllData(4, false, $this->languageID); ?>
			<?php if (count($data) > 0): ?>
			<div class="outers-list-datahome">
				<div class="row">
					<div class="col-xs-9">
						<div class="titles-category">Uncategorized</div>	
					</div>
					<div class="col-xs-3">
						<a href="<?php echo CHtml::normalizeUrl(array('/artikel/index', 'category'=>-1)); ?>" class="btn btn-default see_other">SEE IN UNCATEGORIZED</a>
					</div>
				</div> <div class="clear height-20"></div>
				<div class="list-item-facilities-data news_homedata">
					<div class="row">
							<?php foreach ($data as $key => $value): ?>
								<div class="col-xs-3">
									<div class="paddings">
										<div class="item prelatif">
											<div class="pic">
												<a href="<?php echo CHtml::normalizeUrl(array('/artikel/detail', 'id'=>$value->id, 'url'=>Slug::create($value->title))); ?>">
												<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(641,434, '/images/artikel/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $value->title ?>">
											</a>
											</div>
											<div class="clear height-20"></div>
											<div class="w220 tengah text-gothic h115">
												<div class="title"><?php echo ucwords( $value->title ); ?></div> <div class="clear"></div>
												<div class="desc d-none">
												<p>
													<?php echo substr(strip_tags($value->content), 0, 45).'...'; ?>
												</p>
											</div>
											</div>
											<div class="bc-readmore-item text-gothic"><a href="<?php echo CHtml::normalizeUrl(array('/artikel/detail', 'id'=>$value->id, 'url'=>Slug::create($value->title))); ?>">read more&nbsp;&nbsp;&nbsp; <i class="icon-mr-bt-facilities-item"></i> </a></div>
											<div class="back-shadow-item-fclities"></div>
										</div>	
									</div>
								</div>
							<?php endforeach; ?>
					</div>
				</div>
				<div class="clear height-0"></div>
				<?php if (($k + 1) != count($category)): ?>
				<div class="lines-greys"></div>
				<div class="clear height-30"></div>
				<?php endif ?>
			<!-- end data home news -->
			<?php endif; */ ?>

			<div class="clear"></div>
		</div>
	</div>
	<div class="back-bottom-fcs-grey"></div>


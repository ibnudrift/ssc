<div class="content-inside-about prelatif">
	<div class="clear h140"></div>
	<div class="prelatif container padding-left-30">
		<div class="left breadcumb"><a href="<?php echo CHtml::normalizeUrl(array('home/index')); ?>">Home</a> &gt; <b>Promotions</b></div>
		<div class="clear height-10"></div>
		<div class="clear"></div>
	</div>
	<div class="lines-green"></div>
	<div class="prelatif container margin-left-30">
		<div class="clear height-25"></div>

		<!-- /. start left content -->
		<div class="left w257 left-content">
			<div class="inside">
				<div class="menu-left-inscontent">
					<ul>
						<li><a href="#">Paket Low Back Pain</a></li>
					</ul>
				</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
		<!-- /. End left content -->
		
		<!-- /. start right content -->
		<div class="left w842 right-content">
			<div class="text-content inside">
				<h1 class="title-toppages"><font style="font-weight: normal;">Promotions from</font></h1>
				<div class="clear height-3"></div>
				<h1 class="title-toppages">Surabaya Spine Clinic</h1>
				<div class="clear height-25"></div>

				<div class="list-promotions">
					<?php if ($model): ?>
					<?php foreach ($model as $key => $val): ?>
					<div class="item">
						<div class="row">
							<div class="col-xs-5 w311">
								<div class="pic"><img src="<?php echo Yii::app()->baseUrl; ?>/asset/images/<?php echo $val['img'] ?>" alt=""></div>
							</div>
							<div class="col-xs-7 w528">
								<div class="title text-gothic"><?php echo $val['title'] ?></div>
								<div class="clear height-15"></div>
								<div class="desc text-gothic">
									<?php echo $val['desc'] ?>
								</div>
							</div>
						</div>
					</div>
					<?php endforeach ?>
					<?php endif ?>
					<div class="clear"></div>

				</div>

				<div class="clear height-35"></div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
		<!-- /. End right content -->

		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div class="clear"></div>
		<div class="back-bottom-fcs-grey"></div>
<style type="text/css">
.fancy-info{
	width: 595px;
	height: 330px;
	background-color: #f6f6f6;
	border: 3px solid #9da0a6;
}
.inner-info{
	margin: 0;
	padding: 0; 
}
.box-promo{
	margin: 30px auto 20px;
	width: 410px;
}
.box-promo .namepromo{
	text-align: center;
}
.box-promo .gt-special{
	font-size: 18px;
	font-weight: 400;
	font-family: 'Roboto',sans-serif;
	line-height: 1.3;
	color: rgb(47, 47, 47);
	text-align: center;
	
}
.box-promo .deskbpromo{
	font-size: 13px;
    color: rgb(97, 97, 97);
    text-align: center;
    line-height: 1.4;
	font-family: 'Roboto', sans-serif;
	font-weight: 400;
}
.box-promo .deskbpromo p{
	margin: 0;
	padding: 0;
}
.gt-special a{
	font-size: 16px;
	color: #000;
	text-decoration: none;
}
.gt-special a:hover{
	color: #bfb9a9;
}

</style>
<div class="fancy-info">
	<div class="inner-info">
		<div class="box-promo">
			<div class="namepromo"><img src="<?php echo Yii::app()->baseUrl; ?>/asset/images/logo-bethesda.png" alt="logo bethesda"></div>
			<div style="height: 28px;"></div>
			<div class="gt-special">
				"FREE ADMINISTRATION FEE FOR PHYSIOTHERAPY FROM 1st JANUARY - 28th FEBRUARY 2014"
			</div>
			<div style="height: 18px;"></div>
			<div class="deskbpromo">
			<p> <br>
			Visit our location: <?php echo $this->setting['address1'] ?>
			<br>
			Please call (62 31) 9900 1242
			</p>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</div>
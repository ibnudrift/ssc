<!-- /.start header -->
	<header class="hidden-xs">
		<div class="prelatife">
			<div class="abs-col-box-header">
			<div class="header-top">
				<div class="ins-header container">
					<div class="clear height-10"></div>
					<div class="height-3"></div>
					<div class="row">
						<div class="col-xs-3">
							<div class="clear height-10"></div>
							<div class="logo-header left"><a href="<?php echo CHtml::normalizeUrl(array('home/index', 'lang'=>Yii::app()->language)); ?>"><img src="<?php echo Yii::app()->baseUrl; ?>/asset/images/logo-hd-ssc.png" alt=""></a></div>
						</div>
						<div class="col-xs-9">

							<div class="bx-tp-ourlocandsearch">
								<div class="row">
									<div class="col-xs-9">
										<div class="our-loc">
											<?php
											$urlSekarang = $_GET; 
											$urlSekarang['lang']='en';
											?>
											<?php echo CHtml::link('EN',$this->createUrl($this->route,$urlSekarang), array('style'=>(Yii::app()->language != 'en') ? 'color: #acacac;' : '')); ?>
											<?php $urlSekarang['lang']='id'; ?>
											&nbsp;|&nbsp;
											<?php echo CHtml::link('IN',$this->createUrl($this->route,$urlSekarang), array('style'=>(Yii::app()->language != 'id') ? 'color: #acacac;' : '')); ?>
										</div>
									</div>
									<div class="col-xs-3">
											<div class="frm-top-wacall">
												<a href="#"><img src="<?php echo Yii::app()->baseUrl; ?>/asset/images/new/back-top-wa-call.png" alt="" class="img img-responsive"></a>
											</div>
											<?php 
											/*<div class="frm-serach-top">
												<form action="<?php echo CHtml::normalizeUrl(array('/search/index', 'lang'=>Yii::app()->language)); ?>" method="get">
													<div class="prelatif">
													    <input type="text" class="form-control" name="q"  placeholder="Search">
													  </div>
												</form>
											</div>
											*/ ?>
									</div>
								</div>
							</div>
							<div class="clear height-10"></div>
							
							<div class="menu-top">
								<ul class="list-inline">
								  <li><a href="<?php echo CHtml::normalizeUrl(array('home/index', 'lang'=>Yii::app()->language)); ?>">Home</a></li>
								  <li><a href="<?php echo CHtml::normalizeUrl(array('home/about', 'lang'=>Yii::app()->language)); ?>">About</a></li>
								  <li><a href="<?php echo CHtml::normalizeUrl(array('layanan/index', 'lang'=>Yii::app()->language)); ?>">Services &amp; Facilities</a></li>
								  <li><a href="<?php echo CHtml::normalizeUrl(array('/artikel/index', 'lang'=>Yii::app()->language)); ?>">Articles &amp; Publication</a></li>
								  <li><a href="<?php echo CHtml::normalizeUrl(array('/promotion/index', 'lang'=>Yii::app()->language)); ?>">Promotions</a></li>
								  <li class="last"><a href="<?php echo CHtml::normalizeUrl(array('home/contact', 'lang'=>Yii::app()->language)); ?>">Contact Us</a></li>
								</ul>

							</div>
							<!-- <div class="slogan-header"><img src="<?php echo Yii::app()->baseUrl; ?>/asset/images/slogan-top-header-ssc.png" alt=""></div> -->
							<div class="clear"></div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
			<div class="back-sld-reservation-hd">
				<div id="bx-reservation" class="box-reservation-top enquire-border hden">
					<div class="padding container">
						<div class="height-30"></div>
						<div class="tlt-reservation text-gothic">MAKE A RESERVATION</div>
						<div class="clear height-15"></div>
							<div class="bx-form-reservation">
								<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
								    'type'=>'horizontal',
								    'action'=>array('/home/reservation', 'lang'=>Yii::app()->language),
									'enableAjaxValidation'=>false,
									'clientOptions'=>array(
										'validateOnSubmit'=>false,
									),
									'htmlOptions' => array(
										'enctype' => 'multipart/form-data',
									),
								)); ?>
									<div class="height-10"></div>
									<?php echo $form->errorSummary($this->reservasi); ?>
									<?php if(Yii::app()->user->hasFlash('success')): ?>
									    <?php $this->widget('bootstrap.widgets.TbAlert', array(
									        'alerts'=>array('success'),
									    )); ?>
									<?php endif; ?>
								<div class="row">
									<div class="col-xs-6">
										<!-- first_name -->
										<div class="form-group">
										    <label class="col-sm-4 control-label padding-right-0">First Name</label>
										    <div class="col-sm-7 padding-0">
										      <input type="text" name="ReservationForm[first_name]" value="" class="form-control" required>
										    </div>
										</div>
										<!-- last_name -->
										<div class="form-group">
										    <label class="col-sm-4 control-label padding-right-0">Last Name</label>
										    <div class="col-sm-7 padding-0">
										      <input type="text" name="ReservationForm[last_name]" value="" class="form-control" required>
										    </div>
										</div>
										<!-- email -->
										<div class="form-group">
										    <label class="col-sm-4 control-label padding-right-0">Email Address</label>
										    <div class="col-sm-7 padding-0">
										      <input type="email" name="ReservationForm[email]" value="" class="form-control" required>
										    </div>
										</div>
										<!-- office_number -->
										<div class="form-group">
										    <label class="col-sm-4 control-label padding-right-0">Office Number</label>
										    <div class="col-sm-7 padding-0">
										      <input type="text" name="ReservationForm[office_number]" value="" class="form-control" required>
										    </div>
										</div>
										<!-- mobile_number -->
										<div class="form-group">
										    <label class="col-sm-4 control-label padding-right-0">Mobile Number</label>
										    <div class="col-sm-7 padding-0">
										      <input type="text" name="ReservationForm[mobile_number]" value="" class="form-control" required>
										    </div>
										</div>
										<!-- preferred_day -->
										<div class="form-group">
										    <label class="col-sm-4 control-label padding-right-0">Preferred Day</label>
										    <div class="col-sm-9 padding-0">
										      <!-- <input type="text" name="ReservationForm[preferred_day]" value="" class="form-control" required> -->
										      <label class="checkbox-inline">
												  <input type="checkbox" id="inline_prefrerred" name="ReservationForm[preferred_day][]" value="Monday" checked> Monday
												</label>
												<label class="checkbox-inline">
												  <input type="checkbox" id="inline_prefrerred" name="ReservationForm[preferred_day][]" value="Tuesday"> Tuesday
												</label>
												<label class="checkbox-inline">
												  <input type="checkbox" id="inline_prefrerred" name="ReservationForm[preferred_day][]" value="Wednesday"> Wednesday
												</label>
												<label class="checkbox-inline">
												  <input type="checkbox" id="inline_prefrerred" name="ReservationForm[preferred_day][]" value="Thursday"> Thursday
												</label>
												<div class="clear height-5"></div>
												<label class="checkbox-inline">
												  <input type="checkbox" id="inline_prefrerred" name="ReservationForm[preferred_day][]" value="Friday"> Friday&nbsp;&nbsp;&nbsp;
												</label>
												<label class="checkbox-inline padding-left-23">
												  <!-- <input type="checkbox" id="inline_prefrerred" name="ReservationForm[preferred_day][]" value="Saturday"> Saturday -->
												</label>

										    </div>
										</div>
										<div class="clear height-10"></div>
										<!-- preferred_time -->
										<div class="form-group">
										    <label class="col-sm-4 control-label padding-right-0">Preferred Time</label>
										    <div class="col-sm-7 padding-0">
										      <!-- <input type="text" name="ReservationForm[preferred_time]" value="" class="form-control" required> -->
										      <label class="checkbox-inline">
												  <input type="checkbox" id="inline_prefrerred" name="ReservationForm[preferred_time][]" value="17.00 - 21.00" checked>Tuesday, Wednesday, Friday &gt; 17.00-21.00
												</label>
												<div class="clear"></div>
												<label class="checkbox-inline">
												  <input type="checkbox" id="inline_prefrerred" name="ReservationForm[preferred_time][]" value="19.00 - 21.00"> Monday, Thursday &gt; 19.00 - 21.00
												</label>
										    </div>
										</div>

									</div>
									<div class="col-xs-6">

										<!-- address -->
										<div class="form-group">
										    <label class="col-sm-4 control-label rg-label">Address</label>
										    <div class="col-sm-7 padding-0">
										      <input type="text" name="ReservationForm[address]" value="" class="form-control" required>
										    </div>
										</div>
										<!-- city -->
										<div class="form-group">
										    <label class="col-sm-4 control-label rg-label">City</label>
										    <div class="col-sm-7 padding-0">
										      <input type="text" name="ReservationForm[city]" value="" class="form-control" required>
										    </div>
										</div>
										<!-- state -->
										<div class="form-group">
										    <label class="col-sm-4 control-label rg-label">State</label>
										    <div class="col-sm-7 padding-0">
										      <input type="text" name="ReservationForm[state]" value="" class="form-control" required>
										    </div>
										</div>
										<!-- body -->
										<div class="form-group">
										    <label class="col-sm-4 control-label rg-label">Message</label>
										    <div class="col-sm-7 padding-0">
										      <input type="text" name="ReservationForm[body]" value="" class="form-control">
										    </div>
										</div>

										<div class="clear height-10"></div>
										<div class="form-group">
										    <label class="col-sm-4 control-label rg-label">&nbsp;</label>
										    <div class="col-sm-7 padding-0">
										      <?php $this->widget('bootstrap.widgets.TbButton', array(
														'buttonType'=>'submit',
														// 'type'=>'btn-default',
														'label'=>'SUBMIT',
														'htmlOptions'=> array(
															'class'=>'btn btn-default',
															),
													)); ?>
										    </div>
										</div>

									</div>
									<div class="clear"></div>
								</div>
								<div class="clear height-35"></div>
								<?php $this->endWidget(); ?>
								<div class="clear"></div>
							</div>
						<div class="clear"></div>
					</div>
				</div>
				<div class="back-abs-lnk"><a id="btn-enquire" href="#">MAKE A RESERVATION</a></div>
				<div class="clear"></div>
			</div>
			</div>
		</div>
	</header>

	<div class="heads_mobile visible-xs prelatife">
	<nav class="navbar navbar-default navbar-fixed-top">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="<?php echo CHtml::normalizeUrl(array('home/index', 'lang'=>Yii::app()->language)); ?>">
	        <img src="<?php echo Yii::app()->baseUrl; ?>/asset/images/logo-hd-ssc.png" alt="" class="img-responsive">
	      </a>
	    </div>

	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li><a href="<?php echo CHtml::normalizeUrl(array('home/index', 'lang'=>Yii::app()->language)); ?>">Home</a></li>
	        <li><a href="<?php echo CHtml::normalizeUrl(array('home/about', 'lang'=>Yii::app()->language)); ?>">About</a></li>
	        <li><a href="<?php echo CHtml::normalizeUrl(array('layanan/index', 'lang'=>Yii::app()->language)); ?>">Services &amp; Facilities</a></li>
	        <li><a href="<?php echo CHtml::normalizeUrl(array('/artikel/index', 'lang'=>Yii::app()->language)); ?>">Articles &amp; Publication</a></li>
	        <li><a href="<?php echo CHtml::normalizeUrl(array('/promotion/index', 'lang'=>Yii::app()->language)); ?>">Promotions</a></li>
	        <li class="last"><a href="<?php echo CHtml::normalizeUrl(array('home/contact', 'lang'=>Yii::app()->language)); ?>">Contact Us</a></li>
	      </ul>

	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	<div class="clear clearfix"></div>
</div>

	<!-- /.End header -->
		<script type="text/javascript">
			$('#btn-enquire').live('click', function() {
				if($('.enquire-border').hasClass('hden')==true){
					$('.enquire-border').slideDown('slow').removeClass('hden');
					// $('.header-line').slideUp();
					$(this).html('MAKE A RESERVATION&nbsp;<i class="icon-close-tp-reservation"></i>');
				}else{
					$('.enquire-border').slideUp('slow').addClass('hden');
					// $('.header-line').slideDown();
					$(this).html('MAKE A RESERVATION');
				}
				return false;
			})
			$('.enquire-border').hide().addClass('hden');
		</script>
<?php 
$controllers = $this->id;
$menu_active = $this->action->id;
$n_menu_act = $controllers.'/'.$menu_active;
?>
<?php if ($n_menu_act != 'home/index'): ?>
<div class="nbxs_bottom_fcs_wa visible-xs">
		<div class="prelatife container">
			<div class="in_texts text-center py-3">
				<img src="<?php echo $this->asset_url.'logo_wa_white.png' ?>" alt="" class="img-responsive d-inline align-middle mr-2">
				<p class="m-0 d-inline align-middle"><a href="https://wa.me/6282233227788">WHATSAPP</a></p>
				<div class="clear"></div>
			</div>
		</div>
	</div>
<!-- end fcs -->
<?php endif ?>

<!-- /.start footer -->
<footer class="foot">
	<div class="prelatif container">
		<div class="box-footer-out">
			<div class="py-3"></div>
			<div class="clear height-15"></div>
			<div class="row">
				<div class="col-md-3 col-xs-12 item w238 border-right">
					<div class="menu-footer">
						<ul>
							<li><a href="<?php echo CHtml::normalizeUrl(array('home/index', 'lang'=>Yii::app()->language)); ?>">Home</a></li>
							<li><a href="<?php echo CHtml::normalizeUrl(array('home/about', 'lang'=>Yii::app()->language)); ?>">About</a></li>
							<li><a href="<?php echo CHtml::normalizeUrl(array('artikel/index', 'lang'=>Yii::app()->language)); ?>">Articles & Publication</a></li>
							<li><a href="<?php echo CHtml::normalizeUrl(array('promotion/index', 'lang'=>Yii::app()->language)); ?>">Promotions</a></li>
							<li><a href="<?php echo CHtml::normalizeUrl(array('home/contact', 'lang'=>Yii::app()->language)); ?>">Contact Us</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-3 col-xs-12 item w279 border-right">
					<div class="menu-services-footer padding-left-10">
						<div class="titl-footer">Services & Facilities</div>
						<div class="clear height-10"></div>
						<div class="menu-footer">
							<?php $menusub = Service::Model()->getMenuByCategory(1, 1, 4); ?>
								<!-- <ul> -->
								<?php $this->widget('zii.widgets.CMenu', array(
									    'items'=>$menusub,
									    'encodeLabel'=>false,
									)); ?>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-xs-12 w308 item border-right">
					<div class="connect-social padding-left-10">
						<div class="titl-footer">Connect With Us:</div>
						<div class="clear height-10"></div>
						<dl class="dl-horizontal">
						    <dt><i class="icon-phone-footer"></i></dt>
						    <dd><?php echo ($this->setting['phone']) ?></dd>
						    <div class="clear"></div>
						    <dt><i class="icon-email-footer"></i></dt>
						    <dd><a href="mailto:<?php echo ($this->setting['email']) ?>"><?php echo ($this->setting['email']) ?></a></dd>
						    <div class="clear"></div>
						    <dt><i class="icon-facebook-footer"></i></dt>
						    <dd><a target="_blank" href="<?php echo ($this->setting['facebook']) ?>">spine.surabaya</a></dd>
				    	</dl>
					</div>							
				</div>
				<div class="col-md-3 col-xs-12 w286 item">
					<div class="bx-logo-footer">
						<div class="clear height-2"></div>
						<div class="right" style="margin-right: -10px;">
							<div class="t-top-dwerefooter">
								<div class="left">
									We’re at:
								</div>
								<div class="right">
									Member of:
								</div>
							</div>
							<div class="clear height-10"></div>
							<div class="mg-bt-dwerefooter">
								<img src="<?php echo Yii::app()->baseUrl; ?>/asset/images/logo-footer-up-siloam.jpg" alt=""><a href="http://snei.or.id/" target="_blank"><img src="<?php echo Yii::app()->baseUrl; ?>/asset/images/logo-footer-up-neu.jpg" alt=""></a>
							</div>
						</div>

						<div class="clear height-10"></div>
						<div class="t-copyright">
							<p><?php echo nl2br($this->setting['alamat_footer']) ?></p>
						</div>
						<div class="t-copyright">
							Copyright &copy; 2014. Surabaya Spine Clinic <br>
							All rights reserved. <span class="markdesign">Design by <a href="http://www.markdesign.net/" target="_blank" title="Website Design Surabaya">Mark Design.</a></span>
						</div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear height-25"></div>
</footer>
<!-- /.End footer -->
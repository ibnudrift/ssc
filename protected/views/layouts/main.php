<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="language" content="<?php echo Yii::app()->language ?>" />
	<meta name="keywords" content="<?php echo CHtml::encode($this->metaKey); ?>">
	<meta name="description" content="<?php echo CHtml::encode($this->metaDesc); ?>">

	<?php // Yii::app()->bootstrap->registerCoreCss(); ?>
	<?php // Yii::app()->bootstrap->registerCoreScripts(); ?>

    <link rel="Shortcut Icon" href="<?php echo Yii::app()->baseUrl; ?>/asset/images/favicon.png" />
    <link rel="icon" type="image/ico" href="<?php echo Yii::app()->baseUrl; ?>/asset/images/favicon.png" />
    <link rel="icon" type="image/x-icon" href="<?php echo Yii::app()->baseUrl; ?>/asset/images/favicon.png" />

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/asset/css/screen.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/asset/css/comon.css" />
	
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/asset/js/bootstrap-3/css/bootstrap.min.css" />
	<script type="text/javascript" src="<?php echo Yii::app()->baseUrl; ?>/asset/js/bootstrap-3/js/bootstrap.js"></script>	

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl; ?>/asset/css/styles.css" />
	<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
	<?php $this->widget('application.extensions.fancyapps.EFancyApps', array(
	    'target'=>'',
	    'config'=>array(),
	    )
	); ?>
	

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-scrollto/1.4.4/jquery-scrollto.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    jQuery(document).ready(function($){

        $("#menuscrl_m").live('click', function(){
            var sn_id = $(this).attr('data-id');
            $('html, body').animate({
                scrollTop: $(".ct_mrk_"+ sn_id).offset().top
            }, 1500);
        });
        
    });
    </script>

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-45731129-30', 'auto');
      ga('send', 'pageview');

    </script>
</head>
<body>
	<?php echo $content ?>
</body>
</html>

<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="main-wrapper h100_per">
	<div class="wrapper h100_per">
		<?php echo $this->renderPartial('//layouts/_header', array()); ?>			
		<?php echo $content ?>
		<?php echo $this->renderPartial('//layouts/_footer', array()); ?>
		<div class="clear"></div>
	</div>
</div>
<?php $this->endContent(); ?>


<div class="content-inside-about prelatif">
	<div class="clear h140"></div>
	<div class="prelatif container padding-left-30">
		<div class="left breadcumb"><a href="<?php echo CHtml::normalizeUrl(array('home/index')); ?>">Home</a> &gt; <b>Articles & Publication</b></div>
		<div class="clear height-10"></div>
		<div class="clear"></div>
	</div>
	<div class="lines-green"></div>
	<div class="prelatif container margin-left-30">
		<div class="clear height-25"></div>

		<div class="row">
			<div class="col-md-3">
				<!-- /. start left content -->
				<div class="left-content" style="min-height: 100px;">
					<div class="inside w232">

						<div class="menu-left-inscontent left_custom_news">
							<h3>Category</h3>
							<?php
							$category = Category::model()->findAll();
							?>
							<ul>
								<?php foreach ($category as $key => $value): ?>
								<li><a href="<?php echo CHtml::normalizeUrl(array('/artikel/index', 'category'=>$value->id)); ?>"><?php echo $value->name ?></a></li>
								<?php endforeach ?>
							</ul>
						</div>

						<div class="clear"></div>
					</div>
					<div class="clear"></div>
				</div>
				<!-- /. End left content -->
			</div>
			<div class="col-md-9">
				<!-- /. start right content -->
		<div class="right-content">
			<div class="text-content inside">
				
				<h1 class="title-toppages"><font style="font-weight: normal;">Articles & Publication</font></h1>
				<div class="clear height-3"></div>
				<h1 class="title-toppages">Surabaya Spine Clinic</h1>
				<div class="clear height-25"></div>
				<?php if ($_GET['category'] != ''): ?>
					<?php if ($_GET['category'] == 0): ?>
								<h3>UNCATEGORIZED</h3>
					<?php else: ?>
								<h3>CATEGORY <?php echo Category::model()->findByPk($_GET['category'])->name; ?></h3>
					<?php endif ?>
				<?php endif ?>
								<!-- <p>Sorry, There is no Articles at the moment.</p> -->
				<?php if ($_GET['category'] == ''): ?>
				<?php
				$category = Category::model()->findAll();
				?>

				<!-- starts data home news -->
				<?php foreach ($category as $k => $v): ?>
				<?php
				$_GET['category'] = $v->id;
				?>
				<?php $data = Artikel::model()->getAllData(4, false, $this->languageID); ?>
				<?php if (count($data) > 0): ?>
				
				<div class="outers-list-datahome">
					<div class="row">
						<div class="col-xs-9">
							<div class="titles-category"><?php echo $v->name ?></div>	
						</div>
						<div class="col-xs-3">
							<a href="<?php echo CHtml::normalizeUrl(array('/artikel/index', 'category'=>$v->id)); ?>" class="btn btn-default see_other">SEE IN <?php echo $v->name ?></a>
						</div>
					</div> <div class="clear height-20"></div>
					<div class="list-item-facilities-data news_homedata">
						<div class="row">
								<?php foreach ($data as $key => $value): ?>
									<div class="col-lg-4 col-md-4 col-xs-12">
										<div class="paddings">
											<div class="item prelatif">
												<div class="pic">
													<a href="<?php echo CHtml::normalizeUrl(array('/artikel/detail', 'id'=>$value->id, 'url'=>Slug::create($value->title))); ?>">
													<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(275,186, '/images/artikel/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $value->title ?>">
												</a>
												</div>
												<div class="infos py-3">
													<a href="<?php echo CHtml::normalizeUrl(array('/artikel/detail', 'id'=>$value->id, 'url'=>Slug::create($value->title))); ?>"><h4><?php echo ucwords( $value->title ); ?></h4></a>
													<div class="py-1"></div>
													<a href="<?php echo CHtml::normalizeUrl(array('/artikel/detail', 'id'=>$value->id, 'url'=>Slug::create($value->title))); ?>" class="btn btn-link btns_nmore">Learn More</a>
												</div>
												<?php
												/*
												<div class="clear height-20"></div>
												<div class="w220 tengah text-gothic h115">
													<div class="title"><?php echo ucwords( $value->title ); ?></div> <div class="clear"></div>
													<div class="desc">
													<p>
														<?php echo substr(strip_tags($value->content), 0, 45).'...'; ?>
													</p>
												</div>
												</div>
												<div class="bc-readmore-item text-gothic"><a href="<?php echo CHtml::normalizeUrl(array('/artikel/detail', 'id'=>$value->id, 'url'=>Slug::create($value->title))); ?>">read more&nbsp;&nbsp;&nbsp; <i class="icon-mr-bt-facilities-item"></i> </a></div>
												<div class="back-shadow-item-fclities"></div>
												*/ ?>
											</div>	
										</div>
									</div>
								<?php endforeach; ?>
						</div>
					</div>
					<div class="clear height-0"></div>
					<div class="lines-greys"></div>
					<div class="clear height-30"></div>
					<div class="clear"></div>
				</div>
			<?php endif ?>
			<?php endforeach ?>
			<?php
			$_GET['category'] = -1;
			?>
			<?php $data = Artikel::model()->getAllData(3, false, $this->languageID); ?>
			<?php if (count($data) > 0): ?>
			<div class="outers-list-datahome">
				<div class="row">
					<div class="col-xs-9">
						<div class="titles-category">Uncategorized</div>	
					</div>
					<div class="col-xs-3">
						<a href="<?php echo CHtml::normalizeUrl(array('/artikel/index', 'category'=>-1)); ?>" class="btn btn-default see_other">SEE IN UNCATEGORIZED</a>
					</div>
				</div> <div class="clear height-20"></div>
				<div class="list-item-facilities-data news_homedata">
					<div class="row">
							<?php foreach ($data as $key => $value): ?>
								<div class="col-xs-4">
									<div class="paddings">
										<div class="item prelatif">
											<div class="pic">
												<a href="<?php echo CHtml::normalizeUrl(array('/artikel/detail', 'id'=>$value->id, 'url'=>Slug::create($value->title))); ?>">
												<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(275,186, '/images/artikel/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $value->title ?>">
											</a>
											</div>
											<div class="clear height-20"></div>
											<div class="w220 tengah text-gothic h115">
												<div class="title"><?php echo ucwords( $value->title ); ?></div> <div class="clear"></div>
												<div class="desc">
												<p>
													<?php echo substr(strip_tags($value->content), 0, 45).'...'; ?>
												</p>
											</div>
											</div>
											<div class="bc-readmore-item text-gothic"><a href="<?php echo CHtml::normalizeUrl(array('/artikel/detail', 'id'=>$value->id, 'url'=>Slug::create($value->title))); ?>">read more&nbsp;&nbsp;&nbsp; <i class="icon-mr-bt-facilities-item"></i> </a></div>
											<div class="back-shadow-item-fclities"></div>
										</div>	
									</div>
								</div>
							<?php endforeach; ?>
					</div>
				</div>
				<div class="clear height-0"></div>
				<?php if (($k + 1) != count($category)): ?>
				<div class="lines-greys"></div>
				<div class="clear height-30"></div>
				<?php endif ?>
			<?php endif ?>
			<!-- end data home news -->
<?php else: ?>
				<?php if (count($data) > 0): ?>
					
				<div class="list-item-facilities-data">
					<div class="row">
							<?php foreach ($data as $key => $value): ?>
								<div class="col-xs-4">
									<div class="item prelatif">
										<div class="pic">
											<a href="<?php echo CHtml::normalizeUrl(array('/artikel/detail', 'id'=>$value->id, 'url'=>Slug::create($value->title))); ?>">
											<img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(275,186, '/images/artikel/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $value->title ?>">
										</a>
										</div>
										<div class="clear height-20"></div>
										<div class="w220 tengah text-gothic h115">
											<div class="title"><?php echo ucwords( $value->title ); ?></div>
											<!-- <div class="clear height-15"></div> -->
											<div class="desc"><p><?php echo substr(strip_tags($value->content), 0, 45) ; ?>...</p></div>
										</div>
										<div class="bc-readmore-item text-gothic"><a href="<?php echo CHtml::normalizeUrl(array('/artikel/detail', 'id'=>$value->id, 'url'=>Slug::create($value->title))); ?>">read more&nbsp;&nbsp;&nbsp; <i class="icon-mr-bt-facilities-item"></i> </a></div>
										<div class="back-shadow-item-fclities"></div>
									</div>	
								</div>
							<?php endforeach; ?>
					</div>
				</div>
				<?php else: ?>
				<h3>Data not found</h3>
				<?php endif ?>
<?php endif ?>
				

				<div class="clear height-35"></div>
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
		</div>
		<!-- /. End right content -->
			</div>
		</div>

		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div class="clear"></div>
		<div class="back-bottom-fcs-grey"></div>
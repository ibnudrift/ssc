<?php

class HomeController extends Controller
{

	public function actions()
	{
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
		);
	}	

	public function actionIndex()
	{
		$this->layout='//layouts/column1';

		$this->render('index', array(	
		));
	}

	public function actionAbout()
	{
		$this->pageTitle = 'About Us - Surabaya Spine Clinic - by Dr Eko Agus Subagio, M.D.';
		$this->layout='//layouts/column1';

		$this->render('about', array(	
		));
	}

	public function actionServicedetail()
	{
		$this->layout='//layouts/column1';

		$item = array(
			'layanan_kesehatan'=> 
							array(
								array(
									'img'=>'item-services_1.jpg',
									'title'=>'Trauma fraktur tulang belakang',
									'desc'=>'Trauma fraktur tulang belakang',
									),
								array(
									'img'=>'item-services_2.jpg',
									'title'=>'Tumor tulang belakang',
									'desc'=>'Tumor tulang belakang',
									),
								array(
									'img'=>'item-services_3.jpg',
									'title'=>'Infeksi tulang belakang',
									'desc'=>'Infeksi tulang belakang, tuberculose spine (TBC tulang belakang)',
									),

								array(
									'img'=>'item-services_4.jpg',
									'title'=>'Penyakit degenaratif',
									'desc'=>'Penyakit-penyakit degenaratif: HNP cervical/lumbal, Spondiliosis, Cevicalis, Spondidlistesis.',
									),
								array(
									'img'=>'item-services_5.jpg',
									'title'=>'Deformity',
									'desc'=>'Deformity (deformitas tulang belakang)',
									),
								array(
									'img'=>'item-services_6.jpg',
									'title'=>'Minimally Invasive Spine Surgery',
									'desc'=>'Minimally Invasive Spine Surgery',
									),
								),
				'layanan_pendukung' => array(
								array(
									'img'=>'item-services_4.jpg',
									'title'=>'Reservasi hotel',
									'desc'=>'Reservasi hotel untuk keluarga pasien',
									),
								array(
									'img'=>'item-services_5.jpg',
									'title'=>'Antar jemput pasien',
									'desc'=>'Antar jemput pasien dari luar kota di bandara, stasiun dan/atau terminal',
									),
								array(
									'img'=>'item-services_6.jpg',
									'title'=>'administrasi asuransi',
									'desc'=>'Bantuan pengurusan administrasi asuransi apabila pasien memerlukan',
									),
								),
			);
		
		$id = abs((int) ($_GET['id']));

		$data = $item[$_GET['category']][$id];

		$this->render('servicedetail', array(
			'model' => $item,
			'data'  => $data,
		));
	}

	public function actionNews()
	{
		$this->layout='//layouts/column1';

		$this->pageTitle = 'Articles - '.$this->pageTitle;

		$data = $this->itemNews;

		$this->render('news', array(
			'model'=> $data,
		));
	}

	public function actionNewsdetail()
	{
		$this->layout='//layouts/column1';
		$id = abs( (int) $_GET['id'] );
		$dataAll = $this->itemNews;

		$data = $this->itemNews[$id];

		$this->render('detail_news', array(
			'model' => $dataAll,
			'data' => $data,
		));
	}

	public function actionPromotion()
	{
		$this->layout='//layouts/column1';
		
		$this->pageTitle = 'Promotions - '.$this->pageTitle;

		$it_promotions = array(
					array(
						'img'=>'back-pain.jpg',
						'title'=>'Paket Low Back Pain',
						'desc'=>'<p>Paket Low Back Pain untuk mendeteksi keluhan nyeri pinggang anda, terdiri dari:</p>
								<ol>
									<li>Pemeriksaan fisik oleh dokter Medical Check Up</li>
									<li>MRI Lumbosacral</li>
									<li>Konsultasi Spesial Bedah Saraf dan Spesialis Rehabilitasi Medik</li>
								</ol>
								<p>Dengan harga Rp. <strong>899.000,-</strong></p>
								<p style="margin-bottom: 10px;">Cara untuk mengambil paket Low Back Pain adalah</p>
								<ol>
									<li>Menghubungi CS Siloam Hospital di nomor +6231 503 1333&nbsp;</li>
									<li>DATANG LANGSUNG KE BAGIAN INFORMASI SILOAM HOSPITAL sURABAYA</li>
								</ol>
								<p>Untuk mendapatkan nomor registrasi dan jadwal</p>
								<p>Pada jam pelayanan 08.00-14.00, hari senin s/d sabtu</p>

								',
						),
			);
		// $model = new ContactForm;
		$this->render('promotions', array(
			'model'=>$it_promotions,
		));
	}

	public function actionContact()
	{
		$this->layout='//layouts/column1';

		$this->pageTitle = 'Contact Us - '.$this->pageTitle;

		$model = new ContactForm;
		$model->scenario = 'insert';

		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];

			$status = true;
			$secret_key = "6Ld-huwUAAAAAJ28Q2l1o8BXvWgpwbYT6gKWSiSx";
			$urls_cap = "https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR'];

			$ch = curl_init();
		    curl_setopt($ch, CURLOPT_URL, $urls_cap);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		    $response = curl_exec($ch); 
		    curl_close($ch);

			$response = json_decode($response);
			if($response->success==false)
	        {
	          $status = false;
	          $model->addError('verifyCode', 'Verify you are not robbot');
	        }

			if($status && $model->validate())
			{
				// config email
				$messaged = $this->renderPartial('//mail/contact',array(
					'model'=>$model,
				),TRUE);

				$config = array(
					'to'=>array($model->email, $this->setting['email'], $this->setting['contact_email'], 'deltuariesa@gmail.com'),
					'subject'=>'Hi, Surabaya Spine Clinic Contact from '.$model->email,
					'message'=>$messaged,
				);
				if ($this->setting['contact_cc']) {
					$config['cc'] = array($this->setting['contact_cc']);
				}
				if ($this->setting['contact_bcc']) {
					$config['bcc'] = array($this->setting['contact_bcc']);
				}
				
				Common::mail($config);

				Yii::app()->user->setFlash('success','Thank you for contact us. We will respond to you as soon as possible.');
				$this->refresh();

			}

		}

		$this->render('contact', array(
			'model'=>$model,
		));
	}

	public function actionReservation()
	{
		// $model = new ReservationForm;
		// $model->scenario = 'insert';
		if ($_POST['ReservationForm']){
			// config email
			$messaged = $this->renderPartial('//mail/reservation',array(
				'model'=>$_POST['ReservationForm'],
			),TRUE);

			$config = array(
				'to'=>array($_POST['ReservationForm']['email'], $this->setting['email'], $this->setting['contact_email'], 'deltuariesa@gmail.com'),
				'subject'=>'Reservation From '.$_POST['ReservationForm']['email'],
				'message'=>$messaged,
			);
			if ($this->setting['contact_cc']) {
				$config['cc'] = array($this->setting['contact_cc']);
			}
			if ($this->setting['contact_bcc']) {
				$config['bcc'] = array($this->setting['contact_bcc']);
			}
			// kirim email
			Common::mail($config);
	
			Yii::app()->user->setFlash('msg','Thank you for Reservation us.');
			$this->redirect(array('/home/index'));
		}
	}
	
	public function actionNewsletter()
	{
		if ($_POST['email']) {
			// config email
			$messaged = $this->renderPartial('//mail/subscribe',array(
				'email'=>$_POST['email'],
			),TRUE);

			$config = array(
				'to'=>array($_POST['email'], 'info@surabayaspineclinic.com', 'deltuariesa@gmail.com'),
				'subject'=>'Hi, Surabaya Spine Clinic Request Newsletter from '.$_POST['email'],
				'message'=>$messaged,
			);
			if ($this->setting['contact_cc']) {
				$config['cc'] = array($this->setting['contact_cc']);
			}
			if ($this->setting['contact_bcc']) {
				$config['bcc'] = array($this->setting['contact_bcc']);
			}
			// print_r($config);
			// exit;
			// kirim email
			Common::mail($config);
	
			Yii::app()->user->setFlash('msg','Thank you for subscribe us.');
			$this->redirect(array('/home/index'));
		}
	}


	public function actionLocation()
	{
		$this->layout='//layouts/column1';
		$this->pageTitle = 'Our Location - '.$this->pageTitle;
		// $model = new ContactForm;
		$this->render('location', array(
			// 'model'=>$model,
		));
	}

	public function actionPromo()
	{
		$this->layout='//layoutsAdmin/mainKosong';

		$vwr = $this->renderPartial('//home/promo');

		echo $vwr;
	}

}
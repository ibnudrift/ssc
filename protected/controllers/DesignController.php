<?php

class DesignController extends Controller
{

	public function actionIndex()
	{
		$this->layout='//layouts/column1';

		$this->render('index', array(	
		));
	}

	public function actionAbout()
	{
		$this->layout='//layouts/column1';

		$this->render('about', array(	
		));
	}

}
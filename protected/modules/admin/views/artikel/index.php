<?php
$this->breadcrumbs=array(
	'Artikel',
);

$this->menu=array(
	array('label'=>'Add Artikel', 'icon'=>'th-list','url'=>array('create')),
);
?>

<h1>Artikel</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'promotion-grid',
	'dataProvider'=>$model->search($language_id = 1),
	'filter'=>$model,
	'enableSorting'=>false,
	'columns'=>array(
		array(
            'name'=>'title',
        ),    
		array(
			'name'=>'category_id',
			'filter'=>false,
			'type'=>'raw',
			'value'=>'($data->category_id == "0") ? "Uncotegorized" : Category::model()->findByPk($data->category_id)->name',
		),
		array(
			'name'=>'insert_by',
		),
		array(
			'name'=>'active',
			'filter'=>array(
				'0'=>'Non Active',
				'1'=>'Active',
			),
			'type'=>'raw',
			'value'=>'($data->active == "1") ? "Active" : "Non Active"',
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} {delete}'
		),
	),
)); ?>

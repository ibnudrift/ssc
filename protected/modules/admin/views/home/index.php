<div class="form-signin">
	<div class="lg-backendssc">
		<img src="<?php echo Yii::app()->baseUrl ?>/asset/images/logo-hd-ssc.png" />
	</div>
	<div style="height: 1px;"></div>
	<h3 class="form-signin-heading">Please sign in</h3>
	<?php /** @var BootActiveForm $form */
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	    'id'=>'verticalForm',
	    //'htmlOptions'=>array('class'=>'well'),
		'enableClientValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
		),
	)); ?>
	 
	<?php echo $form->textFieldRow($model, 'username', array('class'=>'span3')); ?>
	<?php echo $form->passwordFieldRow($model, 'password', array('class'=>'span3')); ?>
	<?php echo $form->checkboxRow($model, 'rememberMe'); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'label'=>'Login')); ?>
	 
	<?php $this->endWidget(); ?>
</div>
<style type="text/css">
	.lg-backendssc{
		margin: 0 auto; 
		background-color: #00a2da; 
		-webkit-border-radius: 4px;
		-moz-border-radius: 4px;
		border-radius: 4px; 
		padding: 10px 15px; 
		-webkit-box-shadow: 0px 0px 5px 0px rgba(50, 50, 50, 0.3);
-moz-box-shadow:    0px 0px 5px 0px rgba(50, 50, 50, 0.3);
box-shadow:         0px 0px 5px 0px rgba(50, 50, 50, 0.3);
	}
</style>
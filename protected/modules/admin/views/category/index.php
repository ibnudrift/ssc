<?php
$this->breadcrumbs=array(
	'Product Page'=> array('/admin/page/update', 'id'=>11),
	'Categories',
);

$this->menu=array(
	array('label'=>'Add Category', 'icon'=>'th-list','url'=>array('create')),
);
?>

<h1>Categories</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'category-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		// 'id',
		'name',
		array(
			'header'=>'sort',
			'type'=>'raw',
			'value'=>'SortOrder::sortButton($data,"'.$this->id.'","Category")',
		),
		// array(
		// 	'header'=>'',
		// 	'type'=>'raw',
		// 	'value'=>'
		// 	CHtml::link("<i class=\"icon-zoom-in\"></i>", array("/admin/product/cekproduct", "cat"=>$data->id),array("rel"=>"tooltip", "data-original-title"=>"Lihat Product List"))
		// 	;',
		// ),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} {delete}'
		),
	),
)); ?>

<?php
$this->breadcrumbs=array(
	'Categories'=>array('index'),
	'Add',
);

$this->menu=array(
	array('label'=>'List Category', 'icon'=>'th-list','url'=>array('index')),
);
?>

<h1>Add Category</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
$this->breadcrumbs=array(
	'Categories'=>array('index'),
	// $model->name=>array('view','id'=>$model->id),
	'Edit',
);

$this->menu=array(
	array('label'=>'List Category', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Category', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Category', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<h1>Edit Category <?php echo $model->id; ?></h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
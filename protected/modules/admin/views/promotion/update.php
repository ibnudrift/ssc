<?php
$this->breadcrumbs=array(
	'Promotions'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->menu=array(
	array('label'=>'List Promotion', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Promotion', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Promotion', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<h1>Edit Promotion</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model, 'modelDesc'=>$modelDesc)); ?>
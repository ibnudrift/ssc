<?php
$this->breadcrumbs=array(
	'Setting Bahasa',
);

$this->menu=array(
	// array('label'=>'Add Lang', 'icon'=>'plus-sign', 'url'=>array('create')),
);
?>
<h1>Setting Bahasa</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'setting-form',
    'type'=>'horizontal',
	'enableAjaxValidation'=>false,
	'clientOptions'=>array(
		'validateOnSubmit'=>false,
	),
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php if(Yii::app()->user->hasFlash('success')): ?>
	
	    <?php $this->widget('bootstrap.widgets.TbAlert', array(
	        'alerts'=>array('success'),
	    )); ?>
	
	<?php endif; ?>

	<?php $tabs = array(); ?>
	<?php foreach ($model as $key => $value): ?>
		<?php
		foreach ($value['desc'] as $k => $v) {
			$lang = Language::model()->getName($k);
			if ($value['data']->type=='textarea') {
				$textField = CHtml::textArea('Lang['.$value['data']->name.']['.$lang->code.']', $v->content, array('class'=>'span5'));
			} else {
				$textField = CHtml::textField('Lang['.$value['data']->name.']['.$lang->code.']', $v->content, array('class'=>'span5'));
			}
			if (isset($tabs[$lang->code])) {
				$tabs[$lang->code]['content'] .= '
					<div class="control-group ">
						<label for="Lang_'.$value['data']->name.'_'.$lang->code.'" class="control-label required">'.$value['data']->label.'<span class="required"></span></label>
						<div class="controls">
							'.$textField.'
						</div>
					</div>		
				';
			}else{
				$tabs[$lang->code] = array('label'=>$lang->name, 'content'=>'
					<div class="control-group ">
						<label for="Lang_'.$value['data']->name.'_'.$lang->code.'" class="control-label required">'.$value['data']->label.'<span class="required"></span></label>
						<div class="controls">
							'.$textField.'
						</div>
					</div>		
				'
			        , 'active'=>($k=='id')?TRUE:false,
			    );
			}
		}
		?>
	<?php endforeach ?>
	<?php if (count($tabs)>0): ?>
	<?php $this->widget('bootstrap.widgets.TbTabs', array(
	    'type'=>'tabs', // '', 'tabs', 'pills' (or 'list')
	    'placement'=>'above', // 'above', 'right', 'below' or 'left'
	    'tabs'=>$tabs,
	)); ?>
	<?php endif ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
<script type="text/javascript">
if (typeof RedactorPlugins === 'undefined') var RedactorPlugins = {};

RedactorPlugins.advanced = {
    init: function()
    {
        alert(1);
    }
}
</script>

<?php
$this->breadcrumbs=array(
	'Service'=>array('/admin/service/index'),
	'Service Category'=>array('index'),
	// $model->id=>array('view','id'=>$model->id),
	'Edit',
);

$this->menu=array(
	array('label'=>'List Service Category', 'icon'=>'th-list','url'=>array('index')),
	array('label'=>'Add Service Category', 'icon'=>'plus-sign','url'=>array('create')),
	// array('label'=>'View Service Category', 'icon'=>'pencil','url'=>array('view','id'=>$model->id)),
);
?>

<h1>Edit Service Category</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php echo $this->renderPartial('_form',array('model'=>$model, 'modelDesc'=>$modelDesc)); ?>
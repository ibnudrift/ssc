<?php
$this->breadcrumbs=array(
	'Service'=>array('/admin/service/index'),
	'Service Category'=>array('index'),
	'Add',
);

$this->menu=array(
	array('label'=>'List Service Category', 'icon'=>'th-list','url'=>array('index')),
);
?>

<h1>Add Service Category</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelDesc'=>$modelDesc)); ?>
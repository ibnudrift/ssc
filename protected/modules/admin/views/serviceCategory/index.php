<?php
$this->breadcrumbs=array(
	'Service'=>array('/admin/service/index'),
	'Service Category',
);

$this->menu=array(
	array('label'=>'Add Service Category', 'icon'=>'th-list','url'=>array('create')),
);
?>

<h1>Service Category</h1>
<?php $this->widget('bootstrap.widgets.TbButtonGroup',array('buttons'=>$this->menu,)); ?><br/><br/>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'service-grid',
	'dataProvider'=>$model->search($language_id = 1),
	'filter'=>$model,
	'enableSorting'=>false,
	'columns'=>array(
		array(
            'name'=>'category',
        ),    
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} {delete}'
		),
	),
)); ?>

<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<?php $this->beginWidget('bootstrap.widgets.TbHeroUnit',array(
)); ?>
<h2><?php echo 'Welcome to '.CHtml::encode(Yii::app()->name) ?></h2>

<?php $this->endWidget(); ?>
<div class="row">
	<div class="span12">
		<h3 style="margin: 0;">Page</h3>
		<ul class="thumbnails">
			<?php if (Common::checkAccess('admin.setting.home')): ?>
			<li class="span2">
				<a href="<?php echo CHtml::normalizeUrl(array('/admin/setting/home')) ?>" class="thumbnail">
					<div class="thumbnail">
						<img src="<?php echo Yii::app()->baseUrl ?>/asset/images/icon/home-65.png" alt="">
						<p class="text-tengah less">Home</p>
					</div>
				</a>
			</li>
			<?php endif ?>
			<?php if (Common::checkAccess('admin.setting.about')): ?>
			<li class="span2">
				<a href="<?php echo CHtml::normalizeUrl(array('/admin/setting/about')) ?>" class="thumbnail">
					<div class="thumbnail">
						<img src="<?php echo Yii::app()->baseUrl ?>/asset/images/icon/cutie_icon_set_preview_02.jpg" alt="">
						<p class="text-tengah less">About</p>
					</div>
				</a>
			</li>
			<?php endif ?>
			<?php if (Common::checkAccess('admin.service.index')): ?>
			<li class="span2">
				<a href="<?php echo CHtml::normalizeUrl(array('/admin/service/index')) ?>" class="thumbnail">
					<div class="thumbnail">
						<img src="<?php echo Yii::app()->baseUrl ?>/asset/images/icon/124860-matte-white-square-icon-business-toolset-sc44.png" alt="">
						<p class="text-tengah less">Service</p>
					</div>
				</a>
			</li>
			<?php endif ?>
			<?php if (Common::checkAccess('admin.artikel.index')): ?>
			<li class="span2">
				<a href="<?php echo CHtml::normalizeUrl(array('/admin/artikel/index')) ?>" class="thumbnail">
					<div class="thumbnail">
						<img src="<?php echo Yii::app()->baseUrl ?>/asset/images/icon/cutie_icon_set_preview_08.jpg" alt="">
						<p class="text-tengah less">Artikel</p>
					</div>
				</a>
			</li>
			<?php endif ?>
			<?php if (Common::checkAccess('admin.category.index')): ?>
			<li class="span2">
				<a href="<?php echo CHtml::normalizeUrl(array('/admin/category/index')) ?>" class="thumbnail">
					<div class="thumbnail">
						<img src="<?php echo Yii::app()->baseUrl ?>/asset/images/icon/cutie_icon_set_preview_15.jpg" alt="">
						<p class="text-tengah less">Category</p>
					</div>
				</a>
			</li>
			<?php endif ?>
			<?php if (Common::checkAccess('admin.promotion.index')): ?>
			<li class="span2">
				<a href="<?php echo CHtml::normalizeUrl(array('/admin/promotion/index')) ?>" class="thumbnail">
					<div class="thumbnail">
						<img src="<?php echo Yii::app()->baseUrl ?>/asset/images/icon/Special Promotion.jpg" alt="">
						<p class="text-tengah less">Promotion</p>
					</div>
				</a>
			</li>
			<?php endif ?>
			<?php if (Common::checkAccess('admin.setting.contact')): ?>
			<li class="span2">
				<a href="<?php echo CHtml::normalizeUrl(array('/admin/setting/contact')) ?>" class="thumbnail">
					<div class="thumbnail">
						<img src="<?php echo Yii::app()->baseUrl ?>/asset/images/icon/contact-icon.png" alt="">
						<p class="text-tengah less">Contact</p>
					</div>
				</a>
			</li>
			<?php endif ?>
			<?php if (Common::checkAccess('admin.setting.location')): ?>
			<li class="span2">
				<a href="<?php echo CHtml::normalizeUrl(array('/admin/setting/location')) ?>" class="thumbnail">
					<div class="thumbnail">
						<img src="<?php echo Yii::app()->baseUrl ?>/asset/images/icon/location-24-512.png" alt="">
						<p class="text-tengah less">Our Location</p>
					</div>
				</a>
			</li>
			<?php endif ?>
			<?php if (Common::checkAccess('admin.slide.index')): ?>
			<li class="span2">
				<a href="<?php echo CHtml::normalizeUrl(array('/admin/slide/index')) ?>" class="thumbnail">
					<div class="thumbnail">
						<img src="<?php echo Yii::app()->baseUrl ?>/asset/images/icon/icon-slidekits-xl.png" alt="">
						<p class="text-tengah less">Slide</p>
					</div>
				</a>
			</li>
			<?php endif ?>
			<?php if (Common::checkAccess('admin.setting.index')): ?>
			<li class="span2">
				<a href="<?php echo CHtml::normalizeUrl(array('/admin/setting/index')) ?>" class="thumbnail">
					<div class="thumbnail">
						<img src="<?php echo Yii::app()->baseUrl ?>/asset/images/icon/cutie_icon_set_preview_19.jpg" alt="">
						<p class="text-tengah less">Setting Umum</p>
					</div>
				</a>
			</li>
			<?php endif ?>
		</ul>

	</div>

</div>
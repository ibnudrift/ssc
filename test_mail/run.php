<?php
	require_once('PHPMailer/PHPMailerAutoload.php');
	
	$mail = new PHPMailer;
	$mail->SMTPDebug = 1;                               // Enable verbose debug output
	$mail->isSMTP();                                    // Set mailer to use SMTP

	/* -------------------------- Konfigurasi Dasar ---------------------------------- */
	$mail->SMTPAuth 	= true;                        				// Enable SMTP authentication
	$mail->Host         = 'mail.surabayaspineclinic.com'; // Specify main and backup SMTP servers
    $mail->Port         = 26;                                      // TCP port to connect to
    //$mail->SMTPSecure   = 'tls';                                    // Enable TLS encryption, `ssl` also accepted
    $mail->Username = "no-reply@surabayaspineclinic.com"; // SMTP username
    $mail->Password = "1q2w3e4r5t6y"; // SMTP password 

	$mail->From 		= 'info@surabayaspineclinic.com';
	$mail->FromName 	= 'Sender Info Surabaya';
	$mail->addAddress( 'ibnu@markdesign.net', 'Markdesign Ibnu');   	// Add a recipient

	/* -------------------------- Konfigurasi Tambahan ---------------------------------- */
	//$mail->addAddress('ellen@example.com');           		// Name is optional
	//$mail->addReplyTo('info@example.com', 'Information');
	//$mail->addCC('cc@example.com');
	//$mail->addBCC('bcc@example.com');

	//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
	//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

	/* -------------------------- Konfigurasi Isi ---------------------------------- */
	$mail->isHTML(true);                                  	// Set email format to HTML
	$mail->Subject 	= 'Subject Pengiriman Email Testing';
	$mail->Body 	= '<p>Isi dari Email Testing</p>';
	$mail->AltBody	= 'This is the body in plain text for non-HTML mail clients';
	

	if(!$mail->send()) {
		echo '<br/>Message could not be sent.';
		echo '<br/>Mailer Error: ' . $mail->ErrorInfo;
	} else {
		echo '<br/>Message has been sent';
	}
?>